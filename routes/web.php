<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/transactions', [TransactionController::class, 'index']);

// Menampilkan formulir pembuatan transaksi
Route::get('/transactions/create', [TransactionController::class, 'create']);

// Menyimpan transaksi baru ke dalam basis data
Route::post('/transactions', [TransactionController::class, 'store']);

// Menampilkan detail transaksi
Route::get('/transactions/{transaction}', [TransactionController::class, 'show']);

// Menampilkan formulir pengeditan transaksi
Route::get('/transactions/{transaction}/edit', [TransactionController::class, 'edit']);

// Mengupdate transaksi yang ada
Route::put('/transactions/{transaction}', [TransactionController::class, 'update']);

// Menghapus transaksi
Route::delete('/transactions/{transaction}', [TransactionController::class, 'destroy']);
