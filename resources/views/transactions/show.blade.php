@extends('layouts.app') <!-- Jika Anda menggunakan layout -->

@section('content')
    <h1>Detail Transaksi</h1>

    <ul>
        <li>ID: {{ $transaction->id }}</li>
        <li>Kurir: {{ $transaction->courier_id }}</li>
        <li>Pelanggan: {{ $transaction->customer_id }}</li>
        <li>Jumlah: {{ $transaction->amount }}</li>
        <li>Tanggal Mulai: {{ $transaction->start_date }}</li>
        <li>Tanggal Selesai: {{ $transaction->end_date }}</li>
        <li>Status: {{ $transaction->status }}</li>
    </ul>

    <a href="{{ route('transactions.index') }}">Kembali ke Daftar Transaksi</a>
@endsection
