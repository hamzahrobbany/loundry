@extends('layouts.app') <!-- Jika Anda menggunakan layout -->

@section('content')
    <h1>Edit Transaksi</h1>

    <!-- Formulir untuk mengedit transaksi -->
    <form action="{{ route('transactions.update', $transaction->id) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="courier_id">Kurir:</label>
        <input type="text" name="courier_id" id="courier_id" value="{{ $transaction->courier_id }}">

        <label for="customer_id">Pelanggan:</label>
        <input type="text" name="customer_id" id="customer_id" value="{{ $transaction->customer_id }}">

        <label for="amount">Jumlah:</label>
        <input type="text" name="amount" id="amount" value="{{ $transaction->amount }}">

        <label for="start_date">Tanggal Mulai:</label>
        <input type="text" name="start_date" id="start_date" value="{{ $transaction->start_date }}">

        <label for="end_date">Tanggal Selesai:</label>
        <input type="text" name="end_date" id="end_date" value="{{ $transaction->end_date }}">

        <label for="status">Status:</label>
        <input type="text" name="status" id="status" value="{{ $transaction->status }}">

        <button type="submit">Simpan Perubahan</button>
    </form>

    <a href="{{ route('transactions.index') }}">Kembali ke Daftar Transaksi</a>
@endsection
