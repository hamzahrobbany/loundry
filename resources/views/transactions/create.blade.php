@extends('layouts.app') <!-- Jika Anda menggunakan layout -->

@section('content')
    <h1>Buat Transaksi Baru</h1>

    <!-- Formulir untuk membuat transaksi baru -->
    <form action="{{ route('transactions.store') }}" method="POST">
        @csrf
        <label for="courier_id">Kurir:</label>
        <input type="text" name="courier_id" id="courier_id">

        <label for="customer_id">Pelanggan:</label>
        <input type="text" name="customer_id" id="customer_id">

        <label for="amount">Jumlah:</label>
        <input type="text" name="amount" id="amount">

        <label for="start_date">Tanggal Mulai:</label>
        <input type="text" name="start_date" id="start_date">

        <label for="end_date">Tanggal Selesai:</label>
        <input type="text" name="end_date" id="end_date">

        <label for="status">Status:</label>
        <input type="text" name="status" id="status">

        <button type="submit">Simpan</button>
    </form>
@endsection
