@extends('layouts.app') <!-- Jika Anda menggunakan layout -->

@section('content')
    <h1>Daftar Transaksi</h1>

    <!-- Tabel untuk menampilkan daftar transaksi -->
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Kurir</th>
                <th>Pelanggan</th>
                <th>Jumlah</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th>Status</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
                <tr>
                    <td>{{ $transaction->id }}</td>
                    <td>{{ $transaction->courier_id }}</td>
                    <td>{{ $transaction->customer_id }}</td>
                    <td>{{ $transaction->amount }}</td>
                    <td>{{ $transaction->start_date }}</td>
                    <td>{{ $transaction->end_date }}</td>
                    <td>{{ $transaction->status }}</td>
                    <td>
                        <a href="{{ route('transactions.show', $transaction->id) }}">Detail</a>
                        <a href="{{ route('transactions.edit', $transaction->id) }}">Edit</a>
                        <form action="{{ route('transactions.destroy', $transaction->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <!-- Tombol untuk membuat transaksi baru -->
    <a href="{{ route('transactions.create') }}">Buat Transaksi Baru</a>
@endsection
