<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $fillable = [
        'nik',
        'name',
        'address',
        'phone',
        'courier_id',
        'point',
        'deposit',
    ];
}
