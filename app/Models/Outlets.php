<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlets extends Model
{
    protected $fillable = [
        'code',
        'name',
        'status',
        'address',
        'phone',
    ];
}
