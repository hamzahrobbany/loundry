<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaction extends Model
{
    protected $fillable = [
        'transaction_id',
        'laundry_price_id',
        'laundry_type_id',
        'qty',
        'price',
        'subtotal',
    ];
}
