<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'courier_id',
        'customer_id',
        'user_id',
        'amount',
        'start_date',
        'end_date',
        'status',
    ];
}
