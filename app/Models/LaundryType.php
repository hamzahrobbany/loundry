<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoundryType extends Model
{
    protected $fillable = [
        'name',
    ];
}
