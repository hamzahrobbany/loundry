<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoundryPrice extends Model
{
    protected $fillable = [
        'name',
        'unit_type',
        'laundry_type_id',
        'price',
        'user_id',
    ];
}
